
import javax.swing.*;

public class parques {
    private int[] dice;
    private String indexOfLargest;
    private String[] playerOrder;
    private String[][] board;
    private String[] blueJail;
    private String[] yellowJail;
    private String[] greenJail;
    private String[] redJail;
    private int[] doubleD;
    private String player;
    private int acuTurno;
    private String[] freeTokenBlue;
    private String[] freeTokenYellow;
    private String[] freeTokenGreen;
    private String[] freeTokenRed;
    private int[] blueToken1;
    private int[] blueToken2;
    private int[] blueToken3;
    private int[] blueToken4;
    private int[] redToken1;
    private int[] redToken2;
    private int[] redToken3;
    private int[] redToken4;
    private int[] yellowToken1;
    private int[] yellowToken2;
    private int[] yellowToken3;
    private int[] yellowToken4;
    private int[] greenToken1;
    private int[] greenToken2;
    private int[] greenToken3;
    private int[] greenToken4;


    //objeto
    public static void main(String[] x) {
        new parques();
    }

    //constructor

    public parques() {

        this.doubleD = new int[2];
        this.dice = new int[4];
        this.indexOfLargest = "";
        this.playerOrder = new String[4];
        this.board = new String[19][19];
        this.blueJail = new String[4];
        this.yellowJail = new String[4];
        this.redJail = new String[4];
        this.greenJail = new String[4];
        this.player = "";
        this.acuTurno = 0;
        this.freeTokenBlue = new String[4];
        this.freeTokenYellow = new String[4];
        this.freeTokenGreen = new String[4];
        this.freeTokenRed = new String[4];
        this.blueToken1 = new int[4];
        this.redToken1 = new int[4];
        this.redToken2 = new int[4];
        this.redToken3 = new int[4];
        this.redToken4 = new int[4];
        this.blueToken2 = new int[4];
        this.blueToken3 = new int[4];
        this.blueToken4 = new int[4];
        this.greenToken1 = new int[4];
        this.greenToken2 = new int[4];
        this.greenToken3 = new int[4];
        this.greenToken4 = new int[4];
        this.yellowToken1 = new int[4];
        this.yellowToken2 = new int[4];
        this.yellowToken3 = new int[4];
        this.yellowToken4 = new int[4];
        this.jail();
        this.random();
        this.higher();
        this.panelInit();
        this.fillfreeToken();
        this.game();
    }

    //métodos

    private void game() {

        this.start();
        this.menuPrincipal();
    }

    //Mensajes Iniciales
    private void start() {
        JOptionPane.showMessageDialog(null, "Bienvenido a tu parqués favorito");
        JOptionPane.showMessageDialog(null, "Ahora haremos el sorteo!!");
        JOptionPane.showMessageDialog(null, "Comienza el jugador " + this.indexOfLargest);
        JOptionPane.showMessageDialog(null, "Y Recuerda al terminar pasar tu turno!!!!");
        this.turno();
    }

    //Menu Principal del juego
    private void menuPrincipal() {
        int opcion;
        do {
            String listado = JOptionPane.showInputDialog("====Opciones====\n" +
                    "1. Ver reglas \n" +
                    "2. Ver tablero \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                    "3. Ver cárcel \n" +
                    "4. Lanzar dados \n" +
                    "5. Pasar turno \n" +
                    "6. Ver fichas fuera de la cárcel \n" +
                    "7. Sacar todas de la cárcel para testeo\n" +
                    "0. Salir \n");
            opcion = Integer.parseInt(listado);
            switch (opcion) {
                case 0:
                    JOptionPane.showMessageDialog(null, "Chao pescaoooo");
                    System.exit(0);
                    break;
                case 1:
                    this.rules();
                    break;
                case 2:
                    this.printPanel();
                    break;
                case 3:
                    this.printJail();
                    break;
                case 4:
                    this.menu4();
                    break;
                case 5:
                    this.turno();
                    JOptionPane.showMessageDialog(null, this.player + "¡¡Es tu turno!!");
                    break;
                case 6:
                    this.printJailFree();
                    break;
                case 7:
                    this.freeJailAllfour();
                    break;
                default:

                    JOptionPane.showMessageDialog(null, "Esa opción NO EXISTE");
            }
        } while (opcion != 0);
    }

    //Muestra las reglas
    private void rules() {
        JOptionPane.showMessageDialog(null,
                "1. Limite de 4 jugadores B Y G R \n" +
                        "2. Comienza el jugador con el dado más alto! \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                        "3. Para salir de la cárcel debe sacar pares (tienes 3 intentos) \n" +
                        "4. Si sacas  1 1  o 6 6 sacas todas de la cárcel, con cualquiera de los otros pares sacas solo 2 \n" +
                        "5. Puedes mover una o dos fichas por turno (Recuerda si mueves una es la suma de los dados) \n" +
                        "6. El ganador es el primero que corone sus 4 fichas!");
    }

    //turno va por color
    private void turno() {
        if (this.acuTurno == 4) {
            this.acuTurno = 0;
        }
        this.player = this.playerOrder[this.acuTurno];
        this.acuTurno++;
    }

    //Menu 4 para lanzar dados, da las diferentes opciones desde pares y fichas en la cárcel
    private void menu4() {

        //Lanza dados
        this.doubleDice();
        if (this.player.equals("B")) {
            if (this.doubleD[0] != this.doubleD[1]) {
                if (this.blueJail[0].equals("B1") && this.blueJail[1].equals("B2") && this.blueJail[2].equals("B3") && this.blueJail[3].equals("B4"))
                    this.throwThreeTimes();
                else
                    this.movementMenuNoPair();
            } else {
                if (this.doubleD[0] == 1 || this.doubleD[0] == 6) {

                    if (this.blueJail[0].equals("**") && this.blueJail[1].equals("**") && this.blueJail[2].equals("**") && this.blueJail[3].equals("**"))
                        this.pair();
                    if (this.blueJail[0].equals("**") || this.blueJail[1].equals("**") || this.blueJail[2].equals("**") || this.blueJail[3].equals("**"))
                        this.freeJailAllfour();
                    if (this.blueJail[0].equals("B1") && this.blueJail[1].equals("B2") && this.blueJail[2].equals("B3") && this.blueJail[3].equals("B4")) {
                        JOptionPane.showMessageDialog(null, "Felicidades sacaste todas de la cárcel!");
                        this.freeJailAllfour();
                    }
                }
                if (this.doubleD[0] == 2 || this.doubleD[0] == 3 || this.doubleD[0] == 4 || this.doubleD[0] == 5) {
                    if (this.blueJail[0].equals("**") || this.blueJail[1].equals("**") || this.blueJail[2].equals("**") || this.blueJail[3].equals("**"))
                        this.pair();
                    else
                        this.pairAllJail();
                }
            }
        }
        if (this.player.equals("Y")) {
            if (this.doubleD[0] != this.doubleD[1]) {
                if (this.yellowJail[0].equals("Y1") && this.yellowJail[1].equals("Y2") && this.yellowJail[2].equals("Y3") && this.yellowJail[3].equals("Y4"))
                    this.throwThreeTimes();
                else
                    this.movementMenuNoPair();
            } else {
                if (this.doubleD[0] == 1 || this.doubleD[0] == 6) {

                    if (this.yellowJail[0].equals("**") && this.yellowJail[1].equals("**") && this.yellowJail[2].equals("**") && this.yellowJail[3].equals("**"))
                        this.pair();
                    if (this.yellowJail[0].equals("**") || this.yellowJail[1].equals("**") || this.yellowJail[2].equals("**") || this.yellowJail[3].equals("**"))
                        this.freeJailAllfour();
                    if (this.doubleD[0] == 1 || this.doubleD[0] == 6) {
                        JOptionPane.showMessageDialog(null, "Felicidades sacaste todas de la cárcel!");
                        this.freeJailAllfour();
                    }
                }

                if (this.doubleD[0] == 2 || this.doubleD[0] == 3 || this.doubleD[0] == 4 || this.doubleD[0] == 5) {
                    if (this.yellowJail[0].equals("**") || this.yellowJail[1].equals("**") || this.yellowJail[2].equals("**") || this.yellowJail[3].equals("**"))
                        this.pair();
                    else
                        this.pairAllJail();
                }
            }

        }
        if (this.player.equals("R")) {
            if (this.doubleD[0] != this.doubleD[1]) {
                if (this.redJail[0].equals("R1") && this.redJail[1].equals("R2") && this.redJail[2].equals("R3") && this.redJail[3].equals("R4"))
                    this.throwThreeTimes();
                else
                    this.movementMenuNoPair();
            } else {
                if (this.doubleD[0] == 1 || this.doubleD[0] == 6) {

                    if (this.redJail[0].equals("**") && this.redJail[1].equals("**") && this.redJail[2].equals("**") && this.redJail[3].equals("**"))
                        this.pair();
                    if (this.redJail[0].equals("**") || this.redJail[1].equals("**") || this.redJail[2].equals("**") || this.redJail[3].equals("**"))
                        this.freeJailAllfour();
                    if (this.doubleD[0] == 1 || this.doubleD[0] == 6) {
                        JOptionPane.showMessageDialog(null, "Felicidades sacaste todas de la cárcel!");
                        this.freeJailAllfour();
                    }
                }
                if (this.doubleD[0] == 2 || this.doubleD[0] == 3 || this.doubleD[0] == 4 || this.doubleD[0] == 5) {
                    if (this.redJail[0].equals("**") || this.redJail[1].equals("**") || this.redJail[2].equals("**") || this.redJail[3].equals("**"))
                        this.pair();
                    else
                        this.pairAllJail();
                }
            }
        }
        if (this.player.equals("G")) {
            if (this.doubleD[0] != this.doubleD[1]) {
                if (this.greenJail[0].equals("G1") && this.greenJail[1].equals("G2") && this.greenJail[2].equals("G3") && this.greenJail[3].equals("G4"))
                    this.throwThreeTimes();
                else
                    this.movementMenuNoPair();
            } else {
                if (this.doubleD[0] == 1 || this.doubleD[0] == 6) {

                    if (this.greenJail[0].equals("**") && this.greenJail[1].equals("**") && this.greenJail[2].equals("**") && this.greenJail[3].equals("**"))
                        this.pair();
                    if (this.greenJail[0].equals("**") || this.greenJail[1].equals("**") || this.greenJail[2].equals("**") || this.greenJail[3].equals("**"))
                        this.freeJailAllfour();
                    if (this.doubleD[0] == 1 || this.doubleD[0] == 6) {
                        JOptionPane.showMessageDialog(null, "Felicidades sacaste todas de la cárcel!");
                        {
                            this.freeJailAllfour();
                        }
                    }
                }
                if (this.doubleD[0] == 2 || this.doubleD[0] == 3 || this.doubleD[0] == 4 || this.doubleD[0] == 5) {
                    if (this.greenJail[0].equals("**") || this.greenJail[1].equals("**") || this.greenJail[2].equals("**") || this.greenJail[3].equals("**"))
                        this.pair();
                    else
                        this.pairAllJail();
                }
            }
        }
    }

    //Al tener fichas el juego, este método hace que se muevan sin sacar par MENU
    private void movementMenuNoPair() {
        JOptionPane.showMessageDialog(null, "Recuerda la ficha que muevas primero es la del dado 1 y la segunda la del dado 2!");

        int opcion;
        do {
            String listado = JOptionPane.showInputDialog("====Opciones====\n" +
                    "1. Mover una ficha \n" +
                    "2. Mover dos fichas \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                    "0. Retornar \n");
            opcion = Integer.parseInt(listado);
            switch (opcion) {
                case 0:
                    this.menuPrincipal();
                    break;
                case 1:
                    this.selectToken(1);
                    break;
                case 2:
                    this.selectToken(2);
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Esa opción NO EXISTE");
            }

        } while (opcion != 0);
    }

    //Es llamado si el usuario tiene todas las fichas en la cárcel y lo deja tirar 2 veces más
    private void throwThreeTimes() {
        int opcion, acu = 0;
        do {

            String listado = JOptionPane.showInputDialog("====Opciones====\n" +
                    "1. Lanzar Dado \n");

            opcion = Integer.parseInt(listado);
            if (opcion == 1) {
                this.doubleDice();
                acu++;
                if (this.doubleD[0] == 1 && this.doubleD[1] == 1 || this.doubleD[0] == 6 && this.doubleD[1] == 6) {
                    JOptionPane.showMessageDialog(null, "Que suerte! sacaste los mejores pares, sacas todas las fichas de la cárcel!");
                    this.freeJailAllfour();
                }
                if (this.doubleD[0] == 2 && this.doubleD[1] == 2 || this.doubleD[0] == 3 && this.doubleD[1] == 3 || this.doubleD[0] == 4 && this.doubleD[1] == 4 || this.doubleD[0] == 5 && this.doubleD[1] == 5) {
                    this.pairAllJail();
                }

            } else {
                JOptionPane.showMessageDialog(null, "Esa opción NO EXISTE");
            }

        } while (acu < 2 && this.doubleD[0] != this.doubleD[1]); //Tira máximo 3 veces o hasta el par
    }

    //Libera 2 de la cárcel
    private void freePair() {
        int acu = 0, numerofila, numeroColumna;

        if (this.player.equals("B")) {
            numerofila = 0;
            numeroColumna = 4;
            for (int i = 0; i < 4; i++) {
                if (!this.blueJail[i].equals("**")) {
                    acu++;
                    if (acu <= 2) {
                        this.freeTokenBlue[i] = this.blueJail[i];
                        this.blueJail[i] = "**";

                        switch (i) {
                            case 0:
                                this.blueToken1[0] = numerofila;
                                this.blueToken1[1] = numeroColumna;
                                break;
                            case 1:
                                this.blueToken2[0] = numerofila;
                                this.blueToken2[1] = numeroColumna;
                                break;
                            case 2:
                                this.blueToken3[0] = numerofila;
                                this.blueToken3[1] = numeroColumna;
                                break;
                            case 3:
                                this.blueToken4[0] = numerofila;
                                this.blueToken4[1] = numeroColumna;
                                break;
                        }

                    } else
                        break;
                }
            }


        }
        if (this.player.equals("Y")) {
            numerofila = 14;
            numeroColumna = 0;
            for (int i = 0; i < 4; i++) {
                if (!this.yellowJail[i].equals("**")) {
                    acu++;
                    if (acu <= 2) {
                        this.freeTokenYellow[i] = this.yellowJail[i];
                        this.yellowJail[i] = "**";
                        switch (i) {
                            case 0:
                                this.yellowToken1[0] = numerofila;
                                this.yellowToken1[1] = numeroColumna;
                                break;
                            case 1:
                                this.yellowToken2[0] = numerofila;
                                this.yellowToken2[1] = numeroColumna;
                                break;
                            case 2:
                                this.yellowToken3[0] = numerofila;
                                this.yellowToken3[1] = numeroColumna;
                                break;
                            case 3:
                                this.yellowToken4[0] = numerofila;
                                this.yellowToken4[1] = numeroColumna;
                                break;
                        }
                    } else
                        break;
                }
            }

        }
        if (this.player.equals("G")) {
            numerofila = 4;
            numeroColumna = 18;
            for (int i = 0; i < 4; i++) {
                if (!this.greenJail[i].equals("**")) {
                    acu++;
                    if (acu <= 2) {
                        this.freeTokenGreen[i] = this.greenJail[i];
                        this.greenJail[i] = "**";
                        switch (i) {
                            case 0:
                                this.greenToken1[0] = numerofila;
                                this.greenToken1[1] = numeroColumna;
                                break;
                            case 1:
                                this.greenToken2[0] = numerofila;
                                this.greenToken2[1] = numeroColumna;
                                break;
                            case 2:
                                this.greenToken3[0] = numerofila;
                                this.greenToken3[1] = numeroColumna;
                                break;
                            case 3:
                                this.greenToken4[0] = numerofila;
                                this.greenToken4[1] = numeroColumna;
                                break;
                        }
                    } else
                        break;
                }
            }

        }
        if (this.player.equals("R")) {
            numerofila = 18;
            numeroColumna = 14;
            for (int i = 0; i < 4; i++) {
                if (!this.redJail[i].equals("**")) {
                    acu++;
                    if (acu <= 2) {
                        this.freeTokenRed[i] = this.redJail[i];
                        this.redJail[i] = "**";
                        switch (i) {
                            case 0:
                                this.redToken1[0] = numerofila;
                                this.redToken1[1] = numeroColumna;
                                break;
                            case 1:
                                this.redToken2[0] = numerofila;
                                this.redToken2[1] = numeroColumna;
                                break;
                            case 2:
                                this.redToken3[0] = numerofila;
                                this.redToken3[1] = numeroColumna;

                                break;
                            case 3:
                                this.redToken4[0] = numerofila;
                                this.redToken4[1] = numeroColumna;
                                break;
                        }

                    } else
                        break;
                }
            }
        }
    }

    //Al tener fichas el juego, este método hace que se muevan sin sacar par
    private void movementMenuNoPair() {
        JOptionPane.showMessageDialog(null, "Recuerda la ficha que muevas primero es la del dado 1 y la segunda la del dado 2!");

        int opcion;
        do {
            String listado = JOptionPane.showInputDialog("====Opciones====\n" +
                    "1. Mover una ficha \n" +
                    "2. Mover dos fichas \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                    "0. Retornar \n");
            opcion = Integer.parseInt(listado);
            switch (opcion) {
                case 0:
                    this.menuPrincipal();
                    break;
                case 1:
                    this.selectToken(1);
                    break;
                case 2:
                    this.selectToken(2);
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Esa opción NO EXISTE");
            }

        } while (opcion != 0);
    }
    private void doubleMove (){
        int opcion, ficha1, ficha2;

        do {
            String listado = JOptionPane.showInputDialog("====Opciones====\n" +
                    "1. Elija la ficha que quiere mover con el dado 1 \n" +
                    "2. Elija la ficha que quiere mover con el dado 2 \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                    "3. Lanzar dos dados \n" +
                    "4. Tirar un dados \n" +
                    "5.  \n" +
                    "0. Salir \n");
            opcion = Integer.parseInt(listado);
            switch(opcion) {
                case 0:
                    JOptionPane.showMessageDialog((Component)null, "Chao pescaoooo");
                    break;
                case 1:
                    ficha1=Integer.parseInt(JOptionPane.showInputDialog("Escriba el número de la ficha que quiere mover: "));
                    break;
                case 2:
                    ficha2=Integer.parseInt(JOptionPane.showInputDialog("Escriba el número de la ficha que quiere mover: "));
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                default:
                    JOptionPane.showMessageDialog((Component)null, "Esa opción NO EXISTE");
            }
        } while(opcion != 0);

    }


    private void random() {
        int rand = -1, posicion = 0;
        boolean NewRandom = true;

        //Aleatorios del 1 al 6 sin repetir dentro del arreglo dice
        while (posicion < this.dice.length && this.dice[posicion] == 0) {

            while (NewRandom) {
                rand = (int) (int) (1 + Math.random() * 6);
                for (int i = 0; i < 4; i++) {
                    if (this.dice[i] == rand) {
                        NewRandom = true;
                        break;
                    }
                    NewRandom = false;
                }
            }
            NewRandom = true;
            this.dice[posicion] = rand;
            posicion++;
        }
    }

    private void higher() {
        int higher = 0;

        //Numero Mayor y Jugador con mayor número
        for (int i = 0; i < 4; i++) {
            if (this.dice[i] > higher) {
                higher = this.dice[i];
                if (i == 0) {
                    this.indexOfLargest = "B";
                    this.playerOrder = new String[]{"B", "Y", "R", "G"};
                }
                if (i == 1) {
                    this.indexOfLargest = "Y";
                    this.playerOrder = new String[]{"Y", "R", "G", "B"};
                }
                if (i == 2) {
                    this.indexOfLargest = "R";
                    this.playerOrder = new String[]{"R", "G", "B", "Y"};

    //Es llamado para mover las fichas
    private void movetoken(int num, String player, int acu, int value) {
        int dicePlus = 0;
        if (value == 1) {
            dicePlus = this.doubleD[0] + this.doubleD[1];
        } else {
            if (acu == 0) {
                dicePlus = this.doubleD[0];
            }
            if (acu == 1) {
                dicePlus = this.doubleD[1];
            }
        }

        switch (player) {
            case "B":

                switch (num) {
                    case 1:
                        this.blueToken1 = this.calcular(this.blueToken1, dicePlus, "B");
                        break;
                    case 2:
                        this.blueToken2 = this.calcular(this.blueToken2, dicePlus, "B");
                        break;
                    case 3:
                        this.blueToken3 = this.calcular(this.blueToken3, dicePlus, "B");
                        break;
                    case 4:
                        this.blueToken4 = this.calcular(this.blueToken4, dicePlus, "B");
                        break;
                }
                break;

            case "R":
                switch (num) {
                    case 1:
                        this.redToken1 = this.calcular(this.redToken1, dicePlus, "R");
                        break;

                    case 2:
                        this.redToken2 = this.calcular(this.redToken2, dicePlus, "R");
                        break;
                    case 3:
                        this.redToken3 = this.calcular(this.redToken3, dicePlus, "R");
                        break;
                    case 4:
                        this.redToken4 = this.calcular(this.redToken4, dicePlus, "R");
                        break;
                }
                break;
            case "Y":
                switch (num) {
                    case 1:
                        this.yellowToken1 = this.calcular(this.yellowToken1, dicePlus, "Y");
                        break;

                    case 2:
                        this.yellowToken2 = this.calcular(this.yellowToken2, dicePlus, "Y");
                        break;
                    case 3:
                        this.yellowToken3 = this.calcular(this.yellowToken3, dicePlus, "Y");
                        break;
                    case 4:
                        this.yellowToken4 = this.calcular(this.yellowToken4, dicePlus, "Y");
                        break;
                }
                break;
            case "G":
                switch (num) {
                    case 1:
                        this.greenToken1 = this.calcular(this.greenToken1, dicePlus, "G");
                        break;
                    case 2:
                        this.greenToken2 = this.calcular(this.greenToken2, dicePlus, "G");
                        break;
                    case 3:
                        this.greenToken3 = this.calcular(this.greenToken3, dicePlus, "G");
                        break;
                    case 4:
                        this.greenToken4 = this.calcular(this.greenToken4, dicePlus, "G");
                        break;
                }
                break;

        }
        this.printPanel();
    }

    //Es llamado para calcular en que posición queda la ficha (calcula posicion inicial y posicion final)
    private int[] calcular(int[] position, int dado, String player) {

        int[] finalPos = new int[4];
        finalPos[2] = position[0];
        finalPos[3] = position[1];
        if (position[0] == 0 && position[1] > 0) {
            if (position[1] > dado) {
                finalPos[1] = position[1] - dado;
                finalPos[0] = 0;
            } else {
                dado = (dado + 1) - position[1];
                finalPos[1] = 0;
                finalPos[0] = dado;
            }
        }

        if (position[0] > 0 && position[1] == 0) {
            if (position[0] + dado >= 18) {
                finalPos[0] = 18;
                finalPos[1] = dado - (17 - position[0]);
            } else {
                finalPos[1] = 0;
                finalPos[0] = position[0] + dado;
            }
        }

        if (position[0] == 18 && position[1] > 0)
            if (position[1] + dado >= 18) {
                finalPos[1] = 18;
                finalPos[0] = 17 - ((dado - 1) - (17 - position[1]));

            } else {
                finalPos[0] = 18;
                finalPos[1] = position[1] + dado;
            }

        if (position[1] == 18 && position[0] > 0)
            if (position[0] - dado < 1) {
                finalPos[0] = 0;
                finalPos[1] = 17 - (dado - position[0]);
            } else {
                finalPos[0] = position[0] - dado;
                finalPos[1] = 18;
            }

        if (player.equals("B")) {
            if ((finalPos[0] == 0 && finalPos[1] < 9 || finalPos[0] > 0 && finalPos[1] == 0) && position[1] > 9) {
                finalPos[0] = 9 - finalPos[1];
                finalPos[1] = 9;
                if (finalPos[0] > 8) {
                    finalPos[0] = 9;
                }
            }
            if (position[1] == 9 && position[0] >= 0) {
                if (position[0] + dado > 8) {
                    finalPos[1] = 9;
                    finalPos[0] = 9;
                } else {
                    finalPos[1] = 9;
                    finalPos[0] = position[0] + dado;
                }
            }
        }
        if (player.equals("Y")) {
            if ((finalPos[0] > 9 && finalPos[1] == 0 || finalPos[0] == 18 && finalPos[1] > 0) && position[0] < 9) {
                finalPos[1] = finalPos[0] - 9;
                finalPos[0] = 9;
                if (finalPos[1] > 8) {
                    finalPos[1] = 9;
                }
            }
            if (position[0] == 9 && position[1] >= 0) {
                if (position[1] + dado > 8) {
                    finalPos[1] = 9;
                    finalPos[0] = 9;
                } else {
                    finalPos[0] = 9;
                    finalPos[1] = position[1] + dado;
                }
            }
        }
        if (player.equals("R")) {
            if ((finalPos[0] == 18 && finalPos[1] > 9 || finalPos[1] == 18 && finalPos[0] < 18) && position[1] < 9) {
                finalPos[0] = 18 - (finalPos[1] - 9);
                finalPos[1] = 9;
                if (finalPos[0] < 10) {
                    finalPos[0] = 9;
                }
            }
            if (position[1] == 9 && position[0] <= 18) {
                if (position[0] - dado < 10) {
                    finalPos[1] = 9;
                    finalPos[0] = 9;
                } else {
                    finalPos[1] = 9;
                    finalPos[0] = position[0] - dado;
                }
            }
        }
        if (player.equals("G")) {
            if ((finalPos[0] < 9 && finalPos[1] == 18 || finalPos[0] == 0 && finalPos[1] < 18) && position[0] > 9) {
                finalPos[1] = 18 - (9 - finalPos[0]);
                finalPos[0] = 9;
                if (finalPos[1] < 10) {
                    finalPos[1] = 9;
                }
            }
            if (position[0] == 9 && position[1] <= 18) {
                if (position[1] - dado < 10) {
                    finalPos[1] = 9;
                    finalPos[0] = 9;
                } else {
                    finalPos[0] = 9;
                    finalPos[1] = position[1] - dado;
                }
            }
        }


        return finalPos;
    }

    //Hace que el jugador elija que ficha quiere mover MENU
    private void selectToken(int value) {

        int opcion, acu = 0;
        if (this.player.equals("B")) {
            do {
                String listado = JOptionPane.showInputDialog("====Seleccione la ficha a mover====\n" +
                        "1. B1 \n" +
                        "2. B2 \n" +
                        "3. B3 \n" +
                        "4. B4 \n" +
                        "0. Salir \n");
                opcion = Integer.parseInt(listado);
                switch (opcion) {

                    case 1:
                        this.movetoken(opcion, "B", acu, value);
                        break;
                    case 2:
                        this.movetoken(opcion, "B", acu, value);
                        break;
                    case 3:
                        this.movetoken(opcion, "B", acu, value);
                        break;
                    case 4:
                        this.movetoken(opcion, "B", acu, value);
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Esa opción NO EXISTE");
                }
                acu++;

            } while (acu < value);
        }
        if (this.player.equals("R")) {
            do {
                String listado = JOptionPane.showInputDialog("====Seleccione la ficha a mover====\n" +
                        "1. R1 \n" +
                        "2. R2 \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                        "3. R3 \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                        "4. R4 \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                        "0. Salir \n");
                opcion = Integer.parseInt(listado);
                switch (opcion) {
                    case 0:
                        this.menuPrincipal();
                        break;
                    case 1:
                        this.movetoken(opcion, "R", acu, value);
                        break;
                    case 2:
                        this.movetoken(opcion, "R", acu, value);
                        break;
                    case 3:
                        this.movetoken(opcion, "R", acu, value);
                        break;
                    case 4:
                        this.movetoken(opcion, "R", acu, value);
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Esa opción NO EXISTE");
                }
                acu++;

            } while (acu < value);
        }
        if (this.player.equals("G")) {
            do {
                String listado = JOptionPane.showInputDialog("====Seleccione la ficha a mover====\n" +
                        "1. G1 \n" +
                        "2. G2 \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                        "3. G3 \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                        "4. G4 \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                        "0. Salir \n");
                opcion = Integer.parseInt(listado);
                switch (opcion) {
                    case 0:
                        this.menuPrincipal();
                        break;
                    case 1:
                        this.movetoken(opcion, "G", acu, value);
                        break;
                    case 2:
                        this.movetoken(opcion, "G", acu, value);
                        break;
                    case 3:
                        this.movetoken(opcion, "G", acu, value);
                        break;
                    case 4:
                        this.movetoken(opcion, "G", acu, value);
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Esa opción NO EXISTE");
                }
                acu++;


            } while (acu < value);
        }
        if (this.player.equals("Y")) {
            do {
                String listado = JOptionPane.showInputDialog("====Seleccione la ficha a mover====\n" +
                        "1. Y1 \n" +
                        "2. Y2 \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                        "3. Y3 \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                        "4. Y4 \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                        "0. Salir \n");
                opcion = Integer.parseInt(listado);
                switch (opcion) {
                    case 0:
                        this.menuPrincipal();
                        break;
                    case 1:
                        this.movetoken(opcion, "Y", acu, value);
                        break;
                    case 2:
                        this.movetoken(opcion, "Y", acu, value);
                        break;
                    case 3:
                        this.movetoken(opcion, "Y", acu, value);
                        break;
                    case 4:
                        this.movetoken(opcion, "Y", acu, value);
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Esa opción NO EXISTE");
                }
                acu++;


            } while (acu < value);
        }


    }

    //Si el jugador sacar par y tiene fichas fuera de la cárcel llama este método MENU
    private void pair() {
        int opcion;
        JOptionPane.showMessageDialog(null, "Sacaste par, que suerte!!\n Puedes volver a tirar pero recuerda solo luego de mover tus fichas o Si tienes sacar fichitas de la cárcel!!\n Y por favor al finalizar recuerda retornar!");
        do {
            String listado = JOptionPane.showInputDialog("====Opciones====\n" +
                    "1. Mover una ficha \n" +
                    "2. Mover dos fichas \n" + //Mensaje: Si tiene todas las fichas en la cárcel, lanzar 3 veces
                    "3. Volver a lanzar \n" +
                    "4. Sacar ficha/as de la cárcel \n" +
                    "0. Retornar \n");

            opcion = Integer.parseInt(listado);
            switch (opcion) {
                case 0:
                    this.menuPrincipal();
                    break;
                case 1:
                    this.selectToken(1);
                    break;
                case 2:
                    this.selectToken(2);
                    break;
                case 3:
                    this.menu4();
                    break;
                case 4:
                    this.freePair();
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Esa opción NO EXISTE");
            }

        } while (opcion != 0);
    }

    //Si tiene todas en la cárcel y saca par llama este método MENU
    private void pairAllJail() {
        int opcion;
        JOptionPane.showMessageDialog(null, "Sacaste par, que suerte!!\n Puedes sacar dos fichas de la cárcel! y volver a lanzar!!");
        do {
            String listado = JOptionPane.showInputDialog("====Opciones====\n" +
                    "1. Sacar ficha/as de la cárcel  \n" +
                    "2. Volver a tirar \n");


            opcion = Integer.parseInt(listado);
            switch (opcion) {

                case 1:
                    this.freePair();
                    break;
                case 2:
                    this.menu4();
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Esa opción NO EXISTE");
            }

        } while (opcion != 0);
    }

    //Da un aleatorio para el dado del 1 al 6 (Para elegir el primer jugador)
    private void random() {
        int rand = -1, posicion = 0;
        boolean NewRandom = true;

        //Aleatorios del 1 al 6 sin repetir dentro del arreglo dice
        while (posicion < this.dice.length && this.dice[posicion] == 0) {

            while (NewRandom) {
                rand = (int) (int) (1 + Math.random() * 6);
                for (int i = 0; i < 4; i++) {
                    if (this.dice[i] == rand) {
                        NewRandom = true;
                        break;
                    }
                    NewRandom = false;
                }
            }
            NewRandom = true;
            this.dice[posicion] = rand;
            posicion++;
        }
    }

    //Analiza que jugador saca el número mayor en los dados
    private void higher() {
        int higher = 0;

        //Numero Mayor y Jugador con mayor número
        for (int i = 0; i < 4; i++) {
            if (this.dice[i] > higher) {
                higher = this.dice[i];
                if (i == 0) {
                    this.indexOfLargest = "B";
                    this.playerOrder = new String[]{"B", "Y", "R", "G"};
                }
                if (i == 1) {
                    this.indexOfLargest = "Y";
                    this.playerOrder = new String[]{"Y", "R", "G", "B"};
                }
                if (i == 2) {
                    this.indexOfLargest = "R";
                    this.playerOrder = new String[]{"R", "G", "B", "Y"};

                }
                if (i == 3) {
                    this.indexOfLargest = "G";
                    this.playerOrder = new String[]{"G", "B", "Y", "R"};
                }
            }
            //Prueba de los números
            if (i == 0)
                System.out.println("El jugador B lanza " + dice[i]);
            if (i == 1)
                System.out.println("El jugador Y lanza " + dice[i]);
            if (i == 2)
                System.out.println("El jugador R lanza " + dice[i]);
            if (i == 3)
                System.out.println("El jugador G lanza " + dice[i]);
            if (dice[i] == 6)
                break;
        }
        //Prueba del mayor
        System.out.println("Comienza el jugador: " + this.indexOfLargest);
    }

    //Lanza el dado doble durante el juego.  Reestructurado para que pida el valor de los dados pero contiene la forma de lanzar aleatorio comentado dentro
    private void doubleDice() {

        this.doubleD[0] = Integer.parseInt(JOptionPane.showInputDialog(null, "Inserte el valor del Dado 1:"));
        this.doubleD[1] = Integer.parseInt(JOptionPane.showInputDialog(null, "Inserte el valor del Dado 1:"));
//        for (int i = 0; i < 2; i++) {
//            this.doubleD[i] = (int) (1 + Math.random() * (6));
//        }
        for (int i = 0; i < 2; i++) {
            System.out.println("Dado" + (i + 1) + ": " + this.doubleD[i]);
        }

        System.out.println("================");
    }

    //Lanza el dado sencillo durante el juego
    private int singleDice() {
        int dice;
        dice = (int) (1 + Math.random() * (6));
        return (dice);
    }

    //Inicializa la matriz del juego
    private void panelInit() {

        //Llenado del panel
        for (int column = 1; column < 18; column++)
            this.board[0][column] = "[        ]";

        for (int fila = 18; fila < 19; fila++)
            for (int column = 1; column < 18; column++) {
                this.board[fila][column] = "[        ]";
            }
        for (int fila = 1; fila < 18; fila++) {
            for (int column = 0; column < 1; column++)
                this.board[fila][column] = "[        ]";
        }
        for (int fila = 1; fila < 18; fila++) {
            for (int column = 1; column < 18; column++)
                this.board[fila][column] = "          ";
        }
        for (int fila = 1; fila < 18; fila++) {
            for (int column = 18; column < 19; column++)
                this.board[fila][column] = "[        ]";
        }

        for (int fila = 0; fila < 1; fila++) {
            for (int column = 0; column < 1; column++)
                this.board[fila][column] = "          ";
        }
        for (int fila = 0; fila < 1; fila++) {
            for (int column = 18; column < 19; column++)
                this.board[fila][column] = "          ";

        }
        for (int fila = 18; fila < 19; fila++) {
            for (int column = 0; column < 1; column++)
                this.board[fila][column] = "          ";
        }
        for (int fila = 18; fila < 19; fila++) {
            for (int column = 18; column < 19; column++)
                this.board[fila][column] = "          ";
        }
        for (int fila = 9; fila < 10; fila++) {
            for (int column = 1; column < 8; column++)
                this.board[fila][column] = "[ " +
                        "       ]";
        }
        for (int fila = 9; fila < 10; fila++) {
            for (int column = 11; column < 18; column++)
                this.board[fila][column] = "[        ]";
        }
        for (int fila = 1; fila < 8; fila++) {
            for (int column = 9; column < 10; column++)
                this.board[fila][column] = "[        ]";
        }

        for (int fila = 11; fila < 18; fila++) {
            for (int column = 9; column < 10; column++)
                this.board[fila][column] = "[        ]";
        }
    }

    //Print del panel
    private void printPanel() {
        //Ficha Azul
        this.board[this.blueToken1[0]][this.blueToken1[1]] = this.board[this.blueToken1[0]][this.blueToken1[1]].substring(0, 1) + "B1" + this.board[this.blueToken1[0]][this.blueToken1[1]].substring(1 + 2);
        this.board[this.blueToken1[2]][this.blueToken1[3]] = this.board[this.blueToken1[2]][this.blueToken1[3]].substring(0, 1) + "  " + this.board[this.blueToken1[2]][this.blueToken1[3]].substring(1 + 2);
        this.board[this.blueToken2[0]][this.blueToken2[1]] = this.board[this.blueToken2[0]][this.blueToken2[1]].substring(0, 3) + "B2" + this.board[this.blueToken2[0]][this.blueToken2[1]].substring(1 + 4);
        this.board[this.blueToken2[2]][this.blueToken2[3]] = this.board[this.blueToken2[2]][this.blueToken2[3]].substring(0, 3) + "  " + this.board[this.blueToken2[2]][this.blueToken2[3]].substring(1 + 4);
        this.board[this.blueToken3[0]][this.blueToken3[1]] = this.board[this.blueToken3[0]][this.blueToken3[1]].substring(0, 5) + "B3" + this.board[this.blueToken3[0]][this.blueToken3[1]].substring(1 + 6);
        this.board[this.blueToken3[2]][this.blueToken3[3]] = this.board[this.blueToken3[2]][this.blueToken3[3]].substring(0, 5) + "  " + this.board[this.blueToken3[2]][this.blueToken3[3]].substring(1 + 6);
        this.board[this.blueToken4[0]][this.blueToken4[1]] = this.board[this.blueToken4[0]][this.blueToken4[1]].substring(0, 7) + "B4" + this.board[this.blueToken4[0]][this.blueToken4[1]].substring(1 + 8);
        this.board[this.blueToken4[2]][this.blueToken4[3]] = this.board[this.blueToken4[2]][this.blueToken4[3]].substring(0, 7) + "  " + this.board[this.blueToken4[2]][this.blueToken4[3]].substring(1 + 8);
        //Ficha Roja
        this.board[this.redToken1[0]][this.redToken1[1]] = this.board[this.redToken1[0]][this.redToken1[1]].substring(0, 1) + "R1" + this.board[this.redToken1[0]][this.redToken1[1]].substring(1 + 2);
        this.board[this.redToken1[2]][this.redToken1[3]] = this.board[this.redToken1[2]][this.redToken1[3]].substring(0, 1) + "  " + this.board[this.redToken1[2]][this.redToken1[3]].substring(1 + 2);
        this.board[this.redToken2[0]][this.redToken2[1]] = this.board[this.redToken2[0]][this.redToken2[1]].substring(0, 3) + "R2" + this.board[this.redToken2[0]][this.redToken2[1]].substring(1 + 4);
        this.board[this.redToken2[2]][this.redToken2[3]] = this.board[this.redToken2[2]][this.redToken2[3]].substring(0, 3) + "  " + this.board[this.redToken2[2]][this.redToken2[3]].substring(1 + 4);
        this.board[this.redToken3[0]][this.redToken3[1]] = this.board[this.redToken3[0]][this.redToken3[1]].substring(0, 5) + "R3" + this.board[this.redToken3[0]][this.redToken3[1]].substring(1 + 6);
        this.board[this.redToken3[2]][this.redToken3[3]] = this.board[this.redToken3[2]][this.redToken3[3]].substring(0, 5) + "  " + this.board[this.redToken3[2]][this.redToken3[3]].substring(1 + 6);
        this.board[this.redToken4[0]][this.redToken4[1]] = this.board[this.redToken4[0]][this.redToken4[1]].substring(0, 7) + "R4" + this.board[this.redToken4[0]][this.redToken4[1]].substring(1 + 8);
        this.board[this.redToken4[2]][this.redToken4[3]] = this.board[this.redToken4[2]][this.redToken4[3]].substring(0, 7) + "  " + this.board[this.redToken4[2]][this.redToken4[3]].substring(1 + 8);
        //Ficha Verde

        this.board[this.greenToken1[0]][this.greenToken1[1]] = this.board[this.greenToken1[0]][this.greenToken1[1]].substring(0, 1) + "G1" + this.board[this.greenToken1[0]][this.greenToken1[1]].substring(1 + 2);
        this.board[this.greenToken1[2]][this.greenToken1[3]] = this.board[this.greenToken1[2]][this.greenToken1[3]].substring(0, 1) + "  " + this.board[this.greenToken1[2]][this.greenToken1[3]].substring(1 + 2);
        this.board[this.greenToken2[0]][this.greenToken2[1]] = this.board[this.greenToken2[0]][this.greenToken1[1]].substring(0, 3) + "G2" + this.board[this.greenToken2[0]][this.greenToken1[1]].substring(1 + 4);
        this.board[this.greenToken2[2]][this.greenToken2[3]] = this.board[this.greenToken2[2]][this.greenToken1[3]].substring(0, 3) + "  " + this.board[this.greenToken2[2]][this.greenToken1[3]].substring(1 + 4);
        this.board[this.greenToken3[0]][this.greenToken3[1]] = this.board[this.greenToken3[0]][this.greenToken3[1]].substring(0, 5) + "G3" + this.board[this.greenToken3[0]][this.greenToken3[1]].substring(1 + 6);
        this.board[this.greenToken3[2]][this.greenToken3[3]] = this.board[this.greenToken3[2]][this.greenToken3[3]].substring(0, 5) + "  " + this.board[this.greenToken3[2]][this.greenToken3[3]].substring(1 + 6);
        this.board[this.greenToken4[0]][this.greenToken4[1]] = this.board[this.greenToken4[0]][this.greenToken4[1]].substring(0, 7) + "G4" + this.board[this.greenToken4[0]][this.greenToken4[1]].substring(1 + 8);
        this.board[this.greenToken4[2]][this.greenToken4[3]] = this.board[this.greenToken4[2]][this.greenToken4[3]].substring(0, 7) + "  " + this.board[this.greenToken4[2]][this.greenToken4[3]].substring(1 + 8);
        //Ficha Amarilla

        this.board[this.yellowToken1[0]][this.yellowToken1[1]] = this.board[this.yellowToken1[0]][this.yellowToken1[1]].substring(0, 1) + "Y1" + this.board[this.yellowToken1[0]][this.yellowToken1[1]].substring(1 + 2);
        this.board[this.yellowToken1[2]][this.yellowToken1[3]] = this.board[this.yellowToken1[2]][this.yellowToken1[3]].substring(0, 1) + "  " + this.board[this.yellowToken1[2]][this.yellowToken1[3]].substring(1 + 2);
        this.board[this.yellowToken2[0]][this.yellowToken2[1]] = this.board[this.yellowToken2[0]][this.yellowToken2[1]].substring(0, 3) + "Y2" + this.board[this.yellowToken2[0]][this.yellowToken2[1]].substring(1 + 4);
        this.board[this.yellowToken2[2]][this.yellowToken2[3]] = this.board[this.yellowToken2[2]][this.yellowToken2[3]].substring(0, 3) + "  " + this.board[this.yellowToken2[2]][this.yellowToken2[3]].substring(1 + 4);
        this.board[this.yellowToken3[0]][this.yellowToken3[1]] = this.board[this.yellowToken3[0]][this.yellowToken3[1]].substring(0, 5) + "Y3" + this.board[this.yellowToken3[0]][this.yellowToken3[1]].substring(1 + 6);
        this.board[this.yellowToken3[2]][this.yellowToken3[3]] = this.board[this.yellowToken3[2]][this.yellowToken3[3]].substring(0, 5) + "  " + this.board[this.yellowToken3[2]][this.yellowToken3[3]].substring(1 + 6);
        this.board[this.yellowToken4[0]][this.yellowToken4[1]] = this.board[this.yellowToken4[0]][this.yellowToken4[1]].substring(0, 7) + "Y4" + this.board[this.yellowToken4[0]][this.yellowToken4[1]].substring(1 + 8);
        this.board[this.yellowToken4[2]][this.yellowToken4[3]] = this.board[this.yellowToken4[2]][this.yellowToken4[3]].substring(0, 7) + "  " + this.board[this.yellowToken4[2]][this.yellowToken4[3]].substring(1 + 8);


        String reset = "\u001B[0m";
        String green = "\033[32m";
        String red = "\033[31m";

        for (int fila = 0; fila < 4; fila++) {
            for (int column = 0; column < 4; column++) {
                String blue = "\033[34m";
                System.out.print(blue + this.board[fila][column] + "\t");
            }
            for (int column = 4; column <= 4; column++) {
                System.out.print(reset + this.board[fila][column] + "\t");
            }
            for (int column = 5; column < 10; column++) {
                String blue = "\033[34m";
                System.out.print(blue + this.board[fila][column] + "\t");
            }

            for (int column = 10; column < 19; column++) {
                System.out.print(green + this.board[fila][column] + "\t");
            }

            System.out.println("\t");
        }
        for (int fila = 4; fila < 5; fila++) {
            for (int column = 0; column < 10; column++) {
                String blue = "\033[34m";
                System.out.print(blue + this.board[fila][column] + "\t");
            }


            for (int column = 10; column < 19; column++) {
                System.out.print(reset + this.board[fila][column] + "\t");
            }

            System.out.println("\t");
        }
        for (int fila = 5; fila < 9; fila++) {
            for (int column = 0; column < 10; column++) {
                String blue = "\033[34m";
                System.out.print(blue + this.board[fila][column] + "\t");
            }


            for (int column = 10; column < 19; column++) {
                System.out.print(green + this.board[fila][column] + "\t");
            }

            System.out.println("\t");
        }
        String yellow = "\033[33m";
        for (int fila = 9; fila < 10; fila++) {
            for (int column = 0; column < 10; column++) {
                System.out.print(yellow + this.board[fila][column] + "\t");
            }

            for (int column = 10; column < 19; column++) {
                System.out.print(green + this.board[fila][column] + "\t");
            }

            System.out.println("\t");
        }

        for (int fila = 10; fila < 14; fila++) {

            for (int column = 0; column < 9; column++) {
                System.out.print(yellow + this.board[fila][column] + "\t");
            }
            for (int column = 9; column < 19; column++) {
                System.out.print(red + this.board[fila][column] + "\t");
            }
            System.out.println("\t");
        }
        for (int fila = 14; fila < 15; fila++) {

            for (int column = 0; column < 9; column++) {
                System.out.print(reset + this.board[fila][column] + "\t");
            }
            for (int column = 9; column < 19; column++) {
                System.out.print(red + this.board[fila][column] + "\t");
            }
            System.out.println("\t");
        }

        for (int fila = 15; fila < 18; fila++) {

            for (int column = 0; column < 9; column++) {
                System.out.print(yellow + this.board[fila][column] + "\t");
            }
            for (int column = 9; column < 19; column++) {
                System.out.print(red + this.board[fila][column] + "\t");
            }
            System.out.println("\t");
        }
        for (int fila = 18; fila < 19; fila++) {

            for (int column = 0; column < 9; column++) {
                System.out.print(yellow + this.board[fila][column] + "\t");
            }

            for (int column = 9; column < 14; column++) {
                System.out.print(red + this.board[fila][column] + "\t");
            }
            for (int column = 14; column < 15; column++) {
                System.out.print(reset + this.board[fila][column] + "\t");
            }
            for (int column = 15; column < 19; column++) {
                System.out.print(red + this.board[fila][column] + "\t");
            }
            System.out.println("\t");
        }


    }

    //Llenado de cárceles
    private void jail() {

        for (int i = 0; i < 4; i++)
            this.blueJail[i] = "B" + (i + 1);
        for (int i = 0; i < 4; i++)
            this.yellowJail[i] = "Y" + (i + 1);
        for (int i = 0; i < 4; i++)
            this.redJail[i] = "R" + (i + 1);
        for (int i = 0; i < 4; i++)
            this.greenJail[i] = "G" + (i + 1);
    }

    //Print fichas Fuera de la cárcel
    private void printJailFree() {
        String reset = "\u001B[0m";
        for (int i = 0; i < 4; i++)
            System.out.print(reset + this.freeTokenGreen[i]);
        System.out.println();
        for (int i = 0; i < 4; i++)
            System.out.print(reset + this.freeTokenRed[i]);
        System.out.println();
        for (int i = 0; i < 4; i++)
            System.out.print(reset + this.freeTokenYellow[i]);
        System.out.println();
        for (int i = 0; i < 4; i++)
            System.out.print(reset + this.freeTokenBlue[i]);
        System.out.println();
    }

    //Print de la cárcel
    private void printJail() {
        String reset = "\u001B[0m";
        for (int i = 0; i < 4; i++)
            System.out.print(reset + this.greenJail[i]);
        System.out.println();
        for (int i = 0; i < 4; i++)
            System.out.print(reset + this.redJail[i]);
        System.out.println();
        for (int i = 0; i < 4; i++)
            System.out.print(reset + this.yellowJail[i]);
        System.out.println();
        for (int i = 0; i < 4; i++)
            System.out.print(reset + this.blueJail[i]);
        System.out.println();

    }

    //Llena arreglo de las fichas libres // antes de su liberacion
    private void fillfreeToken() {
        for (int i = 0; i < 4; i++) {
            this.freeTokenBlue[i] = "**";
        }
        for (int i = 0; i < 4; i++) {
            this.freeTokenRed[i] = "**";
        }
        for (int i = 0; i < 4; i++) {
            this.freeTokenYellow[i] = "**";
        }
        for (int i = 0; i < 4; i++) {
            this.freeTokenGreen[i] = "**";
        }
    }

    //Libera a todas las fichas
    private void freeJailAllfour() {
        int numerofila;
        int numeroColumna;
        if (this.player.equals("B")) {
            numerofila = 0;
            numeroColumna = 4;
            for (int i = 0; i < 4; i++) {
                this.freeTokenBlue[i] = this.blueJail[i];

            }
            if (this.blueJail[0].equals("B1")) {
                this.blueToken1[0] = numerofila;
                this.blueToken1[1] = numeroColumna;
            }
            if (this.blueJail[1].equals("B2")) {
                this.blueToken2[0] = numerofila;
                this.blueToken2[1] = numeroColumna;
            }
            if (this.blueJail[2].equals("B3")) {
                this.blueToken3[0] = numerofila;
                this.blueToken3[1] = numeroColumna;
            }
            if (this.blueJail[3].equals("B4")) {
                this.blueToken4[0] = numerofila;
                this.blueToken4[1] = numeroColumna;
            }

            for (int i = 0; i < 4; i++) {
                this.blueJail[i] = "**";
            }
        }
        if (this.player.equals("R")) {
            numerofila = 18;
            numeroColumna = 14;
            for (int i = 0; i < 4; i++) {
                this.freeTokenRed[i] = this.redJail[i];
            }
            if (this.redJail[0].equals("R1")) {
                this.redToken1[0] = numerofila;
                this.redToken1[1] = numeroColumna;
            }
            if (this.redJail[1].equals("R2")) {
                this.redToken2[0] = numerofila;
                this.redToken2[1] = numeroColumna;
            }
            if (this.redJail[2].equals("R3")) {
                this.redToken3[0] = numerofila;
                this.redToken3[1] = numeroColumna;
            }
            if (this.redJail[3].equals("R4")) {
                this.redToken4[0] = numerofila;
                this.redToken4[1] = numeroColumna;
            }


            for (int i = 0; i < 4; i++) {
                this.redJail[i] = "**";
            }
        }
        if (this.player.equals("Y")) {
            numerofila = 14;
            numeroColumna = 0;
            for (int i = 0; i < 4; i++) {
                this.freeTokenYellow[i] = this.yellowJail[i];
            }
            if (this.yellowJail[0].equals("Y1")) {
                this.yellowToken1[0] = numerofila;
                this.yellowToken1[1] = numeroColumna;
            }
            if (this.yellowJail[1].equals("Y2")) {
                this.yellowToken2[0] = numerofila;
                this.yellowToken2[1] = numeroColumna;
            }
            if (this.yellowJail[2].equals("Y3")) {
                this.yellowToken3[0] = numerofila;
                this.yellowToken3[1] = numeroColumna;
            }
            if (this.yellowJail[3].equals("Y4")) {
                this.yellowToken4[0] = numerofila;
                this.yellowToken4[1] = numeroColumna;
            }

            for (int i = 0; i < 4; i++) {
                this.yellowJail[i] = "**";
            }
        }
        if (this.player.equals("G")) {
            numerofila = 4;
            numeroColumna = 18;

            for (int i = 0; i < 4; i++) {
                this.freeTokenGreen[i] = this.greenJail[i];
            }
            if (this.greenJail[0].equals("G1")) {
                this.greenToken1[0] = numerofila;
                this.greenToken1[1] = numeroColumna;
            }
            if (this.greenJail[1].equals("G2")) {
                this.greenToken2[0] = numerofila;
                this.greenToken2[1] = numeroColumna;
            }
            if (this.greenJail[2].equals("G3")) {
                this.greenToken3[0] = numerofila;
                this.greenToken3[1] = numeroColumna;
            }
            if (this.greenJail[3].equals("G4")) {
                this.greenToken4[0] = numerofila;
                this.greenToken4[1] = numeroColumna;
            }

            for (int i = 0; i < 4; i++) {
                this.greenJail[i] = "**";
            }
        }
        this.freeJailAllThrow();

    }

    //Al liberarlas todas el juegador puede volver a tirar
    private void freeJailAllThrow() {
        JOptionPane.showMessageDialog(null, "Felicidades sacaste todas! ahora puedes volver a tirar!");
        this.menu4();

    }


}
